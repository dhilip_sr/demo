/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DLK 1
 */
public class reg extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String type,name,mobile,email,fname,lname,pass,skey;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        {
            try {
                type=request.getParameter("type");
                name=request.getParameter("name");
                pass=request.getParameter("pass");
                fname=request.getParameter("fname");
                lname=request.getParameter("lname");
                mobile=request.getParameter("mobile");
                email=request.getParameter("email");
                String chars = "abcdefghijklmnopqrstuvwxyz"
                     + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                     + "0123456789!@%$%&^?|~'\"#+="
                     + "\\*/.,:;[]()-_<>";

        final int PW_LENGTH = 5;
        Random rnd = new SecureRandom();
        StringBuilder pass1 = new StringBuilder();
        for (int i = 0; i < PW_LENGTH; i++)
            pass1.append(chars.charAt(rnd.nextInt(chars.length())));        
            skey=pass1.toString();
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/securerevoke","root","root");
                String sql="insert into ureg (name,pass,fname,lname,mobile,email) values ('"+name+"','"+pass+"','"+fname+"','"+lname+"','"+mobile+"','"+email+"')";
                String sql1="insert into dreg (name,pass,fname,lname,mobile,email,skey) values ('"+name+"','"+pass+"','"+fname+"','"+lname+"','"+mobile+"','"+email+"','"+skey+"')";
                if(type.equals("user"))
                {
                PreparedStatement pss=con.prepareStatement(sql);
                int executeUpdate = pss.executeUpdate(); 
                 out.println("<script>"
				+"alert('Details are inserted successfully')"
				+"</script>");
                RequestDispatcher rd=request.getRequestDispatcher("/ulogin.jsp");
		rd.include(request, response);
                }
                else if(type.equals("owner"))
                {
                PreparedStatement pss=con.prepareStatement(sql1);
                int executeUpdate = pss.executeUpdate();    
                 out.println("<script>"
				+"alert('Details are inserted successfully')"
				+"</script>");
                RequestDispatcher rd=request.getRequestDispatcher("/dlogin.jsp");
		rd.include(request, response);
                }
                else
                {
                     out.println("<script>"
				+"alert('Not Inserted')"
				+"</script>");
                RequestDispatcher rd=request.getRequestDispatcher("/index.html");
		rd.include(request, response);
                }
            } catch (Exception ex) {
                Logger.getLogger(reg.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
