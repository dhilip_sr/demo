<%-- 
    Document   : update1
    Created on : Sep 19, 2016, 4:16:41 PM
    Author     : DLK 1
--%>

<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.Random"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Secure Data Sharing</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	</head>
	<body class="homepage">
<%
    String name=session.getAttribute("name").toString();
    session.setAttribute("name", name);
    String id=request.getParameter("id");
             session.setAttribute("id",id);
%>
		<!-- Header -->
		<div id="header" style="margin-top: -50px;">
			<div class="container">
				<nav id="nav">
					<ul>
						<li><a href="dhome.jsp">Home</a></li>
						<li><a href="upload.jsp">Upload</a></li>
                                                <li><a href="skey.jsp">Secret Key</a></li>						
						<li class="active"><a href="update.jsp">Update</a></li>
                                                <li><a href="logout.jsp">Logout</a></li>
					</ul>
				</nav>

			</div>
		</div>

		<div id="banner">&nbsp;</div>

		<div id="featured">
			<div class="container">
			 <center>
                                <h1 style="font-size: 30px;">Update Data</h1><br><br><br>
                                  
          <%
             String file="";
          
                        Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/securerevoke", "root", "root");
                        String Query22="select * from file where id='"+id+"'";
                        PreparedStatement ps22=con.prepareStatement(Query22);
                        ResultSet rs22=ps22.executeQuery();
                        if(rs22.next())
                        {
                            file=rs22.getString("fname");         
                        }
                        session.setAttribute("file", file);
                        String jspPath = session.getServletContext().getRealPath("/res");
            String txtFilePath = "C://files//"+file;
           
            BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
            StringBuilder sb = new StringBuilder();
            String line;
//            session.setAttribute("filepath", "C:/upload/Upload-Edit/");
//            session.setAttribute("filen", a2);
            while((line = reader.readLine())!= null){
                 sb.append(line+"\n");
                }
            String book=sb.toString();
        %>
        <form action="update2.jsp" method="post">
            <textarea cols="50" rows="15"  name="test"  > <%=book%>  </textarea>
         <input type="submit" value="Save" />
        </form>
          <br><br>
   
     
        </center> 
                         
                      
			</div>
		</div>
		
		<div id="copyright">
			<div class="container">
				Designed by : ABC Tech  
			</div>
		</div>

	</body>
</html>