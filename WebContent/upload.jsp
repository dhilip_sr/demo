<%-- 
    Document   : upload
    Created on : Sep 19, 2016, 3:18:45 PM
    Author     : DLK 1
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.Random"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Secure Data Sharing</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	</head>
	<body class="homepage">
<%
    String name=session.getAttribute("name").toString();
    session.setAttribute("name", name);
%>
		<!-- Header -->
		<div id="header" style="margin-top: -50px;">
			<div class="container">
				<nav id="nav">
					<ul>
						<li><a href="dhome.jsp">Home</a></li>
						<li class="active"><a href="upload.jsp">Upload</a></li>
                                                <li><a href="skey.jsp">Secret Key</a></li>						
						<li><a href="update.jsp">Update</a></li>
                                                <li><a href="logout.jsp">Logout</a></li>
					</ul>
				</nav>

			</div>
		</div>

		<div id="banner">&nbsp;</div>

		<div id="featured">
			<div class="container">
			 <center>
                                <h1 style="font-size: 30px;">Data Provider Upload File</h1><br><br>                                                     
                                <form action="Url" method="post" enctype="multipart/form-data">        
     <br>
         <strong>UserName:&emsp;&emsp;&emsp;&emsp;&nbsp;</strong><input type="text" name="username" value="<%=name%>" /><br><br>          
               <%
               Class.forName("com.mysql.jdbc.Driver");
               Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/securerevoke","root","root");
               String sql="select * from dreg where name='"+name+"' ";
               PreparedStatement pss=con.prepareStatement(sql);
               String a="";
               ResultSet rss=pss.executeQuery();
               if(rss.next())
               {
                   a=rss.getString("skey");
              %>
          <strong>Data Provider Key:&nbsp;</strong>
          <input type="text" name="key" value="<%=a%>" /><br><br>
    &emsp;&emsp; &emsp; &nbsp;&emsp;<strong>Upload Your File:&emsp;&nbsp;&nbsp;</strong>
         <input type="file" name="file"  /><br><br>          
   &emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="submit" name="Submit" value="Upload" style="background-color: #1da3e9; font-weight: bold;"/>
     <br><br>
      </form>  
      <%
               }
      %>
                            </center>	
			</div>
		</div>
		
		<div id="copyright">
			<div class="container">  
			</div>
		</div>

	</body>
</html>