<%-- 
    Document   : shome
    Created on : Sep 19, 2016, 3:08:16 PM
    Author     : DLK 1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Secure Data Sharing</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	</head>
	<body class="homepage">

		<!-- Header -->
		<div id="header" style="margin-top: -50px;">
			<div class="container">
				<nav id="nav">
					<ul>
						<li class="active"><a href="shome.jsp">Home</a></li>
						<li><a href="owners1.jsp">Owners</a></li>
                                                <li><a href="users1.jsp">Users</a></li>	
                                                <li><a href="files1.jsp">Files</a></li>
                                                <li><a href="trans.jsp">Transactions</a></li>
                                                <li><a href="skey2.jsp">Keys</a></li>
						<li><a href="unrevoke.jsp">Un Revoke</a></li>
                                                <li><a href="attackers1.jsp">View Attackers</a></li>
                                                <li><a href="logout.jsp">Logout</a></li>
					</ul>
				</nav>

			</div>
		</div>

		<div id="banner">&nbsp;</div>

		<div id="featured">
			<div class="container">
			 <center>
                                <h1 style="font-size: 30px;">Storage Provider Home</h1><br><br><br>
                            
                         
                               
                            </center>	
			</div>
		</div>
		
		<div id="copyright">
			<div class="container">
				Designed by : ABC Tech  
			</div>
		</div>

	</body>
</html>